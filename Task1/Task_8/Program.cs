﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_8
{
    class Program
    {
        static void Main(string[] args)
        {
           Console.WriteLine("Enter words.");
            string inputString = String.Empty;
            string firstChar= String.Empty;
            do
            {
                Console.Write("Enter new word and press button Enter:");
                inputString = Console.ReadLine();
                try
                {
                    firstChar = MyReader.GetFirst(inputString);
                    Console.WriteLine(firstChar);
                }
                catch(IndexOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            while (inputString!="@Exit");
          
        }
    }
}
