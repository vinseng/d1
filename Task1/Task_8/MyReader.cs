﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_8
{
    public class MyReader
    {
        private const string _resultString = "Result:{0}";
        public static string GetFirst(string inputString)
        {
            return String.Format(_resultString,inputString[0]);
        }
    }
}
