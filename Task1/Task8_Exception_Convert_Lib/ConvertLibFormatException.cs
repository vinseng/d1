﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Task8_Exception_Convert_Lib
{
    [Serializable]
    public class ConvertLibFormatException:Exception
    {
        public ConvertLibFormatException()
        {

        }
        public ConvertLibFormatException(string message):base(message)
        {

        }
        public ConvertLibFormatException(string message, Exception inner) : base(message,inner)
        {

        }
        public ConvertLibFormatException(string message, Exception inner, string m) : base(message, inner)
        {
            InputString = m;
        }
        protected ConvertLibFormatException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
        private string _inputString;
        public string InputString
        {
            get
            {
                return _inputString;
            }
            private set
            {
                _inputString = value;
            }
        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("InputString", this.InputString);
        }
    }
}
