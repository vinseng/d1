﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Task8_Exception_Convert_Lib
{
    [Serializable]
    public class ConvertLibOverflowException:Exception
    {
        public ConvertLibOverflowException()
        {

        }
        public ConvertLibOverflowException(string message):base(message)
        {

        }
        public ConvertLibOverflowException(string message,Exception inner) : base(message,inner)
        {

        }
        public ConvertLibOverflowException(string message, Exception inner,string m) : base(message, inner)
        {
            InputString = m;
        }
        protected ConvertLibOverflowException(SerializationInfo info,  StreamingContext context) : base(info, context)
        {

        }
        private string _inputString;
        public string InputString
        {
            get
            {
                return _inputString;
            }
             private set
            {
                _inputString = value;
            }
        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("InputString",this.InputString);
        }
    }
}
