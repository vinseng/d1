﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Task8_Exception_Convert_Lib
{
    [Serializable]
    public class ConvertLibArgumentNullException:Exception
    {
        public ConvertLibArgumentNullException()
        {

        }
        public ConvertLibArgumentNullException(string message):base(message)
        {

        }
        public ConvertLibArgumentNullException(string message, Exception inner) : base(message,inner)
        {

        }
        public ConvertLibArgumentNullException(string message, Exception inner, string m) : base(message, inner)
        {
            InputString = m;
        }
        protected ConvertLibArgumentNullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
        private string _inputString;
        public string InputString
        {
            get
            {
                return _inputString;
            }
            private set
            {
                _inputString = value;
            }
        }
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("InputString", this.InputString);
        }
    }
}
