﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace Task8_Exception_Convert_Lib
{
    public class IntConverter:IConverter
    {
        private const string _intPattern = @"[^0-9,-]";
        public  int IntPase(string inputString)
        {
            int result=0;
            try
            {

                if (String.IsNullOrEmpty(inputString))
                {
                    throw new ConvertLibArgumentNullException("Empty  string!");
                }
                if (Regex.IsMatch(inputString, _intPattern))
                {
                    throw new ConvertLibFormatException("InvalidFormat of input String!");
                }

                result = Convert.ToInt32(inputString);
            }

            catch (OverflowException ex)
            {
                throw new ConvertLibOverflowException("Owerflow!" + Environment.NewLine + ex.Message, ex, inputString);
               
            }
            return result;
        }
        
    }
}
