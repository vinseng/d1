﻿namespace Task6
{
    partial class FileVisit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btGetInfo = new System.Windows.Forms.Button();
            this.tbInfo = new System.Windows.Forms.TextBox();
            this.tbEventResults = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btGetInfo
            // 
            this.btGetInfo.Location = new System.Drawing.Point(534, 41);
            this.btGetInfo.Name = "btGetInfo";
            this.btGetInfo.Size = new System.Drawing.Size(75, 23);
            this.btGetInfo.TabIndex = 0;
            this.btGetInfo.Text = "GetInfo";
            this.btGetInfo.UseVisualStyleBackColor = true;
            this.btGetInfo.Click += new System.EventHandler(this.btGetInfo_Click);
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(45, 41);
            this.tbInfo.Multiline = true;
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(460, 129);
            this.tbInfo.TabIndex = 1;
            // 
            // tbEventResults
            // 
            this.tbEventResults.Location = new System.Drawing.Point(45, 196);
            this.tbEventResults.Multiline = true;
            this.tbEventResults.Name = "tbEventResults";
            this.tbEventResults.Size = new System.Drawing.Size(460, 186);
            this.tbEventResults.TabIndex = 2;
            // 
            // FileVisit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 394);
            this.Controls.Add(this.tbEventResults);
            this.Controls.Add(this.tbInfo);
            this.Controls.Add(this.btGetInfo);
            this.Name = "FileVisit";
            this.Text = "Task6";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btGetInfo;
        private System.Windows.Forms.TextBox tbInfo;
        private System.Windows.Forms.TextBox tbEventResults;
    }
}

