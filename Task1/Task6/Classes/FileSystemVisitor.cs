﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Task6
{
    public delegate bool MatchSearch(string partOfName);
    public class FileSystemVisitor:IFileSystemVisitor<string>
    {
        public FileSystemVisitor()
        {
            _filter = null;
        }
        public FileSystemVisitor(Func<string, bool>  filter)
        {
            _filter = filter;
        }
        private Func<string,bool> _filter;
        public event EventHandler<SearchEventArgs> FilteredDirectoryFinded;
        public event EventHandler<SearchEventArgs> FilteredFileFinded;
        public event EventHandler<SearchEventArgs> DirectoryFinded;
        public event EventHandler<SearchEventArgs> FileFinded;
        public event EventHandler<SearchEventArgs> Started;
        public event EventHandler<SearchEventArgs> Finished;
        protected virtual void OnFilteredFileFinded(SearchEventArgs args)
        {
            var tmp = FilteredFileFinded;
            if (tmp != null)
            {
                tmp(this, args);
            }
        }
        protected virtual void OnFilteredDirectoryFinded(SearchEventArgs args)
        {
            var tmp = FilteredDirectoryFinded;
            if (tmp != null)
            {
                tmp(this, args);
            }
        }
        protected virtual void OnDirectoryFinded(SearchEventArgs args)
        {
            var tmp = DirectoryFinded;
            if (tmp != null)
            {
                tmp(this, args);
            }
        }
        protected virtual void OnFileFinded(SearchEventArgs args)
        {
            var tmp = FileFinded;
            if (tmp != null)
            {
                tmp(this, args);
            }
        }
        protected virtual void OnFinished(SearchEventArgs args)
        {
            var tmp = Finished;
            if (tmp != null)
            {
                tmp(this, args);
            }
        }
        protected virtual void OnStarted(SearchEventArgs args)
        {
            var tmp = Started;
            if(tmp!=null)
            {
                tmp(this, args);
            }
        }
        private bool Filter(string inputstring)
        {
           return (_filter == null) ? true : _filter(inputstring);
        }
        private IEnumerable<string> GetMyFileList(DirectoryInfo startPoint)
        {
            FileInfo[] files = null;
            DirectoryInfo[] subDirs = null;
            files = startPoint.GetFiles("*.*");
            if (startPoint != null)
            {
                OnDirectoryFinded(new SearchEventArgs(startPoint.FullName));
            }
            if (Filter(startPoint.FullName))
            {
                SearchEventArgs e = new SearchEventArgs(startPoint.FullName);
                OnFilteredDirectoryFinded(e);
                if (e.Stop) yield break;
                if (!e.Handled)
                {
                    yield return startPoint.FullName;
                }
            }
            foreach (System.IO.FileInfo fi in files)
            {
                OnFileFinded(new SearchEventArgs(fi.FullName));
                if (Filter(fi.FullName))
                {
                    SearchEventArgs e = new SearchEventArgs(fi.FullName);
                    OnFilteredFileFinded(e);
                    if (e.Stop) yield break;
                    if (!e.Handled)
                    {
                        yield return fi.FullName;
                    }
                }
            }
            subDirs = startPoint.GetDirectories();
            foreach (System.IO.DirectoryInfo dirInfo in subDirs)
            {

                foreach (var i in GetMyFileList(dirInfo))
                {
                    //if (Filter(i))
                    //{
                        yield return i;
                    //}
                }
            }
        }
        public IEnumerable<string> Generator(DirectoryInfo startPoint)
        {
            OnStarted(new SearchEventArgs());
            foreach (var itm in GetMyFileList(startPoint))
            {
                yield return itm;
            }
            OnFinished(new SearchEventArgs());
        }
        public static bool StopSearh(string name)
        {
            return false;
        }
        public static bool Handled(string name)
        {
            return false;
        }

    }
}
