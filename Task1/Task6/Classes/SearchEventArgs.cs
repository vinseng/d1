﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    public class SearchEventArgs:EventArgs
    {
        public SearchEventArgs()
        {

        }
        public SearchEventArgs(string name)
        {
            _name = name;
        }
        private bool _handled;
        private string _name;
        private bool _stop;
        public bool Handled
        {
            get
            {
                return _handled;
            }
            set
            {
                _handled = value;
            }
        }
        public  bool Stop
        {
            get
            {
                return _stop;
            }
            set
            {
                _stop = value;
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
    }
}
