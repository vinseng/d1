﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task6
{
    public interface IFileSystemVisitor<T>
    {
        IEnumerable<T> Generator(System.IO.DirectoryInfo startPoint);
    }
}
