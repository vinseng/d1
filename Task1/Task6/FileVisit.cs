﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Task6
{
    public partial class FileVisit : Form
    {
        public FileVisit()
        {
            InitializeComponent();
            _fv = new FileSystemVisitor(a=>a.Contains("1"));
            _fv.FilteredDirectoryFinded += OnFilteredDirectoryFinded;
            _fv.FilteredFileFinded += OnFilteredFileFinded;
            _fv.Started += OnStarted;
            _fv.Finished += OnFinished;
            _fv.FileFinded += OnFileFinded;
            _fv.DirectoryFinded += OnDirectoryFinded;
        }
       // private const string rn = Environment.NewLine;
        private FileSystemVisitor _fv;
        private void WriteMessage(string message)
        {
            tbEventResults.Text += message+ Environment.NewLine;
        }
        private void OnDirectoryFinded(object sender, SearchEventArgs args)
        {
           
            WriteMessage("Directory Finded!");
        }
        private void OnFileFinded(object sender, SearchEventArgs args)
        {
           
            WriteMessage("File Finded!");
        }
        private void OnFinished(object sender, SearchEventArgs args)
        {
            
            WriteMessage("Work is finished!");
        }
        private void OnStarted(object sender, SearchEventArgs args)
        {
           
            WriteMessage("Work is started!");
        }
        private void OnFilteredDirectoryFinded(object sender,SearchEventArgs args)
        {
            
            WriteMessage("DirectoryFinded" + args.Name);
            args.Stop= FileSystemVisitor.StopSearh(args.Name);
            args.Handled = FileSystemVisitor.Handled(args.Name) ;
        }
        private void OnFilteredFileFinded(object sender, SearchEventArgs args)
        {
           
            WriteMessage("FileFinded" + args.Name);
            args.Stop = FileSystemVisitor.StopSearh(args.Name);
            args.Handled = FileSystemVisitor.Handled(args.Name);
        }
        private void btGetInfo_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            FolderBrowserDialog fb = new FolderBrowserDialog();
            if (fb.ShowDialog() == DialogResult.OK)
            {
                foreach (var o in _fv.Generator(new DirectoryInfo(@fb.SelectedPath)))
                {
                    sb.Append(o + Environment.NewLine);
                    
                }

                tbInfo.Text = sb.ToString();
            }
        }
        protected override void Dispose(bool disposing)
        {
            _fv.FilteredDirectoryFinded -= OnFilteredDirectoryFinded;
            _fv.FilteredFileFinded -= OnFilteredFileFinded;
            _fv.Started -= OnStarted;
            _fv.Finished -= OnFinished;
            _fv.FileFinded -= OnFileFinded;
            _fv.DirectoryFinded -= OnDirectoryFinded;
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
