﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_BCL
{
    public interface IFileProcessor
    {
        void BackupFile(string path, string pathTo);
        string ReadFile(string path,IRuEnConverter converter);
        bool NeedSaveChange();
    }
}
