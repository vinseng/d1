﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
namespace Task3_BCL
{
    public interface IFileBrowser
    {
        IList<string> GetFileInformation(OpenFileDialog dial);
        IList<string> GetFileInformation(FolderBrowserDialog dial);
        string GetFileList(IList<string> inputList);
    }
}
