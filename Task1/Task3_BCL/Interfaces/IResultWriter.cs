﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Task3_BCL
{
    public interface IResultWriter
    {
        void WriteResults(object textOtBox,string textResult);
    }
}
