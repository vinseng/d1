﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_BCL
{
    public interface IRuEnConverter
    {
        string ConvertToEn(string s);
        bool IsFixable(string inputString);
    }
}
