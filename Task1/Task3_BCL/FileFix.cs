﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;
namespace Task3_BCL
{
    public partial class FileFix : Form
    {
        //public FileFix()
        //{
        //    InitializeComponent();
        //    _filesList = new List<string>();
        //}
        public FileFix(IFileBrowser fb,IFileProcessor fp,IResultWriter rw,IRuEnConverter ruEn)
        {
            InitializeComponent();
            _fb = fb;
            _fp = fp;
            _rw = rw;
            _ruEn = ruEn;
        }
        private IFileBrowser _fb;
        private IFileProcessor _fp;
        private static IResultWriter _rw;
        private IRuEnConverter _ruEn;
        private const string _extStr = "*.txt;*.csv";
        private List<string> _filesList;
        

        private void btnFileName_Click(object sender, EventArgs e)
        {
            _rw.WriteResults(tbResult, String.Empty);
            _filesList =_fb.GetFileInformation(new OpenFileDialog()) as List<string>;
            if(_filesList!=null)
            {
                _rw.WriteResults(this.tbFileName,_fb.GetFileList(_filesList));
            }
            
        }

        private void btFolderName_Click(object sender, EventArgs e)
        {
            _rw.WriteResults(tbResult, String.Empty);
            _filesList = _fb.GetFileInformation(new FolderBrowserDialog()) as List<string>;
            if (_filesList != null)
            {
                _rw.WriteResults(this.tbFolderName, _fb.GetFileList(_filesList));
            }
            
        }

        
        private void btDo_Click(object sender, EventArgs e)
        {
           
            _rw.WriteResults(tbResult, String.Empty);
            foreach (string path in _filesList)
            {
                _rw.WriteResults(tbResult, _fp.ReadFile(path,_ruEn));
                if(_fp.NeedSaveChange())
                {
                    var result = MessageBox.Show("Хотите сохранить изменения?", "Сохранить",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        _fp.BackupFile(path, @"Backup");
                    }
                }
            }
        }
    }
}
