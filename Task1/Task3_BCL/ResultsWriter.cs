﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Task3_BCL
{
    public class ResultsWriter:IResultWriter
    {
        public void WriteResults(object texBox,string textResult)
        {
            if (texBox != null)
            {
                if (texBox is TextBox)
                {
                    ((TextBox)texBox).Text += textResult;
                }
            }
        }

    }
}
