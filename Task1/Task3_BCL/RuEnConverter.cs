﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_BCL
{
    public class RuEnConverter: IRuEnConverter
    {
        private static Dictionary<string, string> Replacements = new Dictionary<string, string>()
        {
            {"А","А"},{"а","a"},{"С","C"},{"с","c"},{"Х","X"},{"х","x"},{"Н","H"},{"В","B"},{"О","O"},{"о","o"},{"Е","E"},{"е","e"},{"Т","T"},{"К","K"},
            {"Р","P"},{"к","k"}

        };
        public bool IsFixable(string inputString)
        {
            return Replacements.ContainsKey(inputString);
        }
        public  string ConvertToEn(string s)
        {
            var sb = new StringBuilder(s);
            foreach (var kvp in Replacements)
                sb.Replace(kvp.Key, kvp.Value);
            return sb.ToString();
        }
    }
}
