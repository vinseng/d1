﻿namespace Task3_BCL
{
    partial class FileFix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.lbFileName = new System.Windows.Forms.Label();
            this.lbFolderName = new System.Windows.Forms.Label();
            this.tbFolderName = new System.Windows.Forms.TextBox();
            this.btnFileName = new System.Windows.Forms.Button();
            this.btFolderName = new System.Windows.Forms.Button();
            this.btDo = new System.Windows.Forms.Button();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbFileName
            // 
            this.tbFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFileName.Location = new System.Drawing.Point(28, 40);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(582, 29);
            this.tbFileName.TabIndex = 0;
            // 
            // lbFileName
            // 
            this.lbFileName.AutoSize = true;
            this.lbFileName.Location = new System.Drawing.Point(25, 9);
            this.lbFileName.Name = "lbFileName";
            this.lbFileName.Size = new System.Drawing.Size(23, 13);
            this.lbFileName.TabIndex = 1;
            this.lbFileName.Text = "File";
            // 
            // lbFolderName
            // 
            this.lbFolderName.AutoSize = true;
            this.lbFolderName.Location = new System.Drawing.Point(25, 92);
            this.lbFolderName.Name = "lbFolderName";
            this.lbFolderName.Size = new System.Drawing.Size(57, 13);
            this.lbFolderName.TabIndex = 2;
            this.lbFolderName.Text = "FolderFiles";
            // 
            // tbFolderName
            // 
            this.tbFolderName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFolderName.Location = new System.Drawing.Point(29, 119);
            this.tbFolderName.Multiline = true;
            this.tbFolderName.Name = "tbFolderName";
            this.tbFolderName.Size = new System.Drawing.Size(581, 84);
            this.tbFolderName.TabIndex = 3;
            // 
            // btnFileName
            // 
            this.btnFileName.Location = new System.Drawing.Point(616, 40);
            this.btnFileName.Name = "btnFileName";
            this.btnFileName.Size = new System.Drawing.Size(35, 23);
            this.btnFileName.TabIndex = 4;
            this.btnFileName.Text = "...";
            this.btnFileName.UseVisualStyleBackColor = true;
            this.btnFileName.Click += new System.EventHandler(this.btnFileName_Click);
            // 
            // btFolderName
            // 
            this.btFolderName.Location = new System.Drawing.Point(616, 119);
            this.btFolderName.Name = "btFolderName";
            this.btFolderName.Size = new System.Drawing.Size(35, 23);
            this.btFolderName.TabIndex = 5;
            this.btFolderName.Text = "...";
            this.btFolderName.UseVisualStyleBackColor = true;
            this.btFolderName.Click += new System.EventHandler(this.btFolderName_Click);
            // 
            // btDo
            // 
            this.btDo.Location = new System.Drawing.Point(616, 232);
            this.btDo.Name = "btDo";
            this.btDo.Size = new System.Drawing.Size(75, 23);
            this.btDo.TabIndex = 6;
            this.btDo.Text = "Go";
            this.btDo.UseVisualStyleBackColor = true;
            this.btDo.Click += new System.EventHandler(this.btDo_Click);
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(29, 232);
            this.tbResult.Multiline = true;
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(581, 122);
            this.tbResult.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 448);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.btDo);
            this.Controls.Add(this.btFolderName);
            this.Controls.Add(this.btnFileName);
            this.Controls.Add(this.tbFolderName);
            this.Controls.Add(this.lbFolderName);
            this.Controls.Add(this.lbFileName);
            this.Controls.Add(this.tbFileName);
            this.Name = "Form1";
            this.Text = "Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label lbFileName;
        private System.Windows.Forms.Label lbFolderName;
        private System.Windows.Forms.TextBox tbFolderName;
        private System.Windows.Forms.Button btnFileName;
        private System.Windows.Forms.Button btFolderName;
        private System.Windows.Forms.Button btDo;
        private System.Windows.Forms.TextBox tbResult;
    }
}

