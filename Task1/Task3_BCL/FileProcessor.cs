﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
namespace Task3_BCL
{
    public class FileProcessor : IFileProcessor
    {
        private bool _needSaveChage = false;
        private StringBuilder sbAllFileStrings;
        private const string _patternRus = @"\w*([А-Я]|[а-я])\w*";
        private const string _rusLetters = @"([А-Я]|[а-я])";
        public void BackupFile(string path, string pathTo)
        {
            try
            {
                string folder = Path.GetDirectoryName(path);
                folder += "\\" + pathTo;
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);
                File.Copy(path, Path.Combine(folder, Path.GetFileName(path)), true);
                File.WriteAllText(path, sbAllFileStrings.ToString(), Encoding.Unicode);

            }
            catch (IOException copyError)
            {
                MessageBox.Show(copyError.Message, "Error!",
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
            }
        }
       
        public bool NeedSaveChange()
        {
            return _needSaveChage;
        }
        private StringBuilder AppendResultString(StringBuilder sb,int counter,int index,string stringValue)
        {
            return sb.Append(String.Format("Ln:{0} Col:{1} Text:{2}", counter, index+1, stringValue));
        }
        public string ReadFile(string path, IRuEnConverter converter)
        {
            int counter = 0;
            int rusSubStringCounter = 0;
            int canBeFixedCounter = 0;
            string line = String.Empty;
            _needSaveChage = false;
            bool canFix = false;
            sbAllFileStrings = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            sb.Append(@path + "\r\n");
            using (StreamReader file = new StreamReader(@path))
            {
                MatchCollection myMatch = null;

                while ((line = file.ReadLine()) != null)
                {
                    counter++;

                    myMatch = Regex.Matches(line, _patternRus);

                    MatchCollection mylittleMatch = null;
                    foreach (Match match in myMatch)
                    {
                        canFix = true;
                        mylittleMatch = Regex.Matches(match.Groups[0].Value, @_rusLetters);
                        foreach (Match match2 in mylittleMatch)
                        {
                            
                            AppendResultString(sb, counter, match2.Groups[0].Index, match.Groups[0].Value);
                            if (converter.IsFixable(match2.Groups[0].Value))
                            {
                                _needSaveChage = true;
                                sb.Append("(Can be Fixed)\r\n");
                            }
                            else
                            {
                                canFix = false;
                                sb.Append("\r\n");
                            }

                        }
                        //правим все возможные русские символы
                        line = new StringBuilder(line).Replace(match.Groups[0].Value, converter.ConvertToEn(match.Groups[0].Value)).ToString();
                        if (canFix) canBeFixedCounter++;
                        rusSubStringCounter++;
                    }
                    sbAllFileStrings.AppendLine(line);

                    //tbResult.Text += sb.ToString();

                }
                sb.Append(String.Format("RusSustrings:{0} CanBeFixed:{1}\r\n", rusSubStringCounter, canBeFixedCounter));

            }
            
            return sb.ToString();
        }
    }
}
