﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Task3_BCL
{
    public class FileBrowser : IFileBrowser
    {
        private static List<string> _extList = new List<string>() { ".txt",".csv"
    };
        public IList<string> GetFileInformation(OpenFileDialog dial)
        {



            dial.Filter = "Files|" +"*"+String.Join<string>(";*",_extList);
            
            if (dial.ShowDialog() == DialogResult.OK)
            {

                return new List<string>() { dial.FileName
            };

            }
            return null;
        }
        public IList<string> GetFileInformation(FolderBrowserDialog dial)
        {
            //FolderBrowserDialog folderBrowseDialog = new FolderBrowserDialog();
            if (dial.ShowDialog() == DialogResult.OK)
            {

                List<string> resultFilesList=(Directory.GetFiles(@dial.SelectedPath, "*.*", SearchOption.TopDirectoryOnly)).ToList<string>();

                if (_extList.Count == 0)
                {
                    return resultFilesList;
                }
                else
                {
                    return resultFilesList.Where(s => _extList.Contains<string>(Path.GetExtension(s))).ToList<string>();
                }
              
                
                
            }
            return null;
        }
        public string GetFileList(IList<string> inputList)
        {
            StringBuilder sb = new StringBuilder("Files:\r\n");
            foreach (string fInfo in inputList)
            {
                sb.Append(String.Format("File name:{0} | File ext:{1} |File size:{2} \r\n", Path.GetFileName(fInfo),
                Path.GetExtension(fInfo),
                new FileInfo(fInfo).Length));
               
            }
            return sb.ToString();
        }
    }

}


