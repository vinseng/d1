﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task8_Exception_Convert_Lib;
namespace Task8_Exception_Convert_Lib_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //IntConverter ic = new IntConverter();
            //int r=ic.IntPase(Console.ReadLine());
            //Console.WriteLine(r);
            //Console.ReadKey();
            IntConverter ic = new IntConverter();
            Console.WriteLine("Enter words.");
            string inputString = String.Empty;
            string firstChar= String.Empty; 
            do
            {
                Console.Write("Enter new word and press button Enter:");
                inputString = Console.ReadLine();
                try
                {
                    int r = ic.IntPase(inputString);
                    Console.WriteLine(r);
                }
                catch (ConvertLibArgumentNullException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (ConvertLibFormatException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch(ConvertLibOverflowException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            while (inputString != "@Exit");

        }
    }
}
