﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using SampleSupport;
using Task.Data;
using System.Text.RegularExpressions;
// Version Mad01

namespace SampleQueries
{
    [Title("LINQ Module")]
    [Prefix("Linq")]
    public class LinqSamples : SampleHarness
    {

        private DataSource dataSource = new DataSource();

        [Category("Restriction Operators")]
        [Title("Task 1")]
        [Description("Task 1")]

        public void Linq1()
        {
            decimal X = 5000;
            //var ordersList = from itm in dataSource.Customers
            //                 select new { CustomerID = itm.CustomerID, CustomerSum = itm.Orders.Sum(a => a.Total) } into g
            //                where g.CustomerSum > X
            //                 select g;
            var ordersList = from itm in dataSource.Customers
                             where itm.Orders.Sum(a => a.Total) > X
                             select itm;


            foreach (var x in ordersList)
            {
                Console.WriteLine(String.Format("CustomerID: {0}", x.CustomerID));
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 2")]
        [Description("Task 2")]

        public void Linq2()
        {

            //var supList = from itmCustomer in dataSource.Customers
            //              from itmSuppliers in dataSource.Suppliers

            //              where itmSuppliers.Country == itmCustomer.Country && itmCustomer.City == itmSuppliers.City
            //              select new { CustomerName =itmCustomer.CustomerID, SupName=itmSuppliers.SupplierName };
            var supList = from itmCustomer in dataSource.Customers
                          from itmSuppliers in dataSource.Suppliers
                          where itmSuppliers.Country == itmCustomer.Country && itmCustomer.City == itmSuppliers.City
                          group new { CustomerName = itmCustomer.CustomerID, SupName = itmSuppliers.SupplierName } by new { CustomerName = itmCustomer.CustomerID }
                          into g
                          select g;
            foreach (var p in supList)
            {
                ObjectDumper.Write(p);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 3")]
        [Description("Task 3")]

        public void Linq3()
        {
            decimal X = 5000;
            var ordersList = from itm in dataSource.Customers
                             where itm.Orders.Any(a => a.Total > X)
                             select itm;
            foreach (var p in ordersList)
            {
                ObjectDumper.Write(p);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 4")]
        [Description("Task 4")]

        public void Linq4()
        {
          
            var ordersList = from itm in dataSource.Customers
                             where itm.Orders.Length > 0
                             select new { CustomerID = itm.CustomerID, StartDate = itm.Orders.Min(a => a.OrderDate) };
            foreach (var p in ordersList)
            {
                ObjectDumper.Write(p);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 5")]
        [Description("Task 5")]

        public void Linq5()
        {

            var customerList = (from itm in dataSource.Customers
                                where itm.Orders.Length > 0
                                select new
                                {
                                    CustomerName = itm.CompanyName,
                                    StartDate = itm.Orders.Min(a => a.OrderDate),
                                    CustomerSum = itm.Orders.Sum(a => a.Total)
                                }).OrderBy(a => a.StartDate.Year)
                             .ThenBy(a => a.StartDate.Month)
                             .ThenByDescending(a => a.CustomerSum)
                             .ThenBy(a => a.CustomerName);

            foreach (var p in customerList)
            {
                ObjectDumper.Write(p);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 6")]
        [Description("Task 6")]

        public void Linq6()
        {

            var customerList = from itm in dataSource.Customers
                               where
                               String.IsNullOrEmpty(itm.Region) == true ||
                               itm.PostalCode == null ||
                               (!String.IsNullOrEmpty(itm.PostalCode) && Regex.Match(itm.PostalCode, "[^ 0 - 9]").Success) ||
                               !Regex.Match(itm.Phone, @"^(\()\b").Success
                               select itm;


            foreach (var p in customerList)
            {
                ObjectDumper.Write(p);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 7")]
        [Description("Task 7")]

        public void Linq7()
        {

            var productsList = from itm in dataSource.Products
                               
                               group itm by itm.Category into g
                               select new
                               {
                                   Category = g.Key,
                                   InStock = from p in g
                                             orderby p.UnitPrice
                                             group p by p.UnitsInStock 
                                           
                                             
                                           
                               };
                              
                              

            foreach (var p in productsList)
            {
                foreach(var pi in p.InStock)
                {
                    ObjectDumper.Write(pi);
                }
                
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 8")]
        [Description("Task 8")]

        public void Linq8()
        {

            var productList = from itm in dataSource.Products
                              group itm by Task.Helper.GetRange(itm.UnitPrice) into g
                              select new { GroupName = g.Key, Products = from p in g select p };




            foreach (var p in productList)
            {
                foreach (var pi in p.Products)
                {
                    Console.WriteLine(String.Format("Group: {0},Product: {1}, Price {2}", p.GroupName,pi.ProductName,pi.UnitPrice));
                }
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 9")]
        [Description("Task 9")]

        public void Linq9()
        {

            var orderList = from itm in dataSource.Customers
                            group itm by itm.City into g
                            orderby g.Key
                            select new { City = g.Key, CityAvg = g.SelectMany(a => a.Orders).Average<Order>(a => a.Total),
                                CityCount = g.SelectMany(a => a.Orders).Count<Order>()/g.Select(b=>b).Count<Customer>()
                            };
                            

            foreach (var p in orderList)
            {
                ObjectDumper.Write(p);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 10")]
        [Description("Task 10")]

        public void Linq10()
        {

            var orderList = from itm in dataSource.Customers
                            group itm by itm into g
                            orderby g.Key.CompanyName
                            select new
                            {
                               Company=g.Key.CompanyName,
                                MonthCount = from o in g.Key.Orders
                                            group o by o.OrderDate.Month into yearGroup
                                            select new
                                            {
                                                Month = yearGroup.Key,
                                                CompanyCount = yearGroup.Count<Order>()
                                            },
                               YearCount =  from o in g.Key.Orders 
                                            group o by o.OrderDate.Year into yearGroup
                                            select new
                                            {
                                                Year = yearGroup.Key,
                                                CompanyCount = yearGroup.Count<Order>()
                                            },
                                YearMonthCount = from o in g.Key.Orders
                                            group o by new { Year = o.OrderDate.Year, Month = o.OrderDate.Month } into yearGroup
                                            select new
                                            {
                                                Year = yearGroup.Key.Year,
                                                Month=yearGroup.Key.Month,
                                                CompanyCount = yearGroup.Count<Order>()
                                            },



                            };


            foreach (var p in orderList)
            {
                //foreach (var pi in p.YearCount)
                //{
                //    Console.WriteLine(String.Format("CompanyName: {0},Year: {1}, CompanyYearCount {2}", p.Company,pi.Year, pi.CompanyCount));
                //}
                foreach (var pi in p.YearMonthCount)
                {
                    Console.WriteLine(String.Format("CompanyName: {0}, Year: {1}, Month:{3}  CompanyYearCount {2}", p.Company, pi.Year, pi.CompanyCount,pi.Month));
                }
            }
        }

    }

}

