﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    public class Helper
    {
        public static string GetRange(decimal price)
        {
            if (price < 15)
            {
                return "Cheap";
            }
            else
            if (price < 50)
            {
                return "Low";
            }
            else
            {
                return "Expensive";
            };

        }
    }
}
