﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
namespace Task4_Collections.SubTask3
{
    public class IntList : IEnumerable<int>
    {
        public IntList(List<int> inputList)
        {
            _list = inputList;
        }
        private List<int> _list;
        IEnumerator<int> IEnumerable<int>.GetEnumerator()
        {

           foreach (var item in MyItr())
            {
                yield return (int)item;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            yield return MyItr();
        }
        public IEnumerable MyItr()
        {
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i] < 0)
                {
                    yield break;
                }
                else
                {
                    yield return _list[i];
                }
            }


        }
    }
}
