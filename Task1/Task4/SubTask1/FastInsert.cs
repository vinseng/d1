﻿using System;
using System.Text;
using System.Collections;
namespace Task4_Collections.SubTask1
{
    public class FastInsert
    {
        public FastInsert()
        {
            _ht = new Hashtable();
        }
        private Hashtable _ht;
        public void Add(string keyWord,string inputString)
        {
            if(!_ht.ContainsKey(keyWord))
            _ht.Add(keyWord, inputString);
        }
        public string Get(string keyWord)
        {
            return _ht[keyWord].ToString();
        }
    }
}
