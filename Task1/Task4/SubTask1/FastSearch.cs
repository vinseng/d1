﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task4_Collections.SubTask1
{
    public class FastSearch
    {
        public FastSearch()
        {
            _fastSearch = new Dictionary<string,string>();
        }

        private Dictionary<string, string> _fastSearch;
        public void Add(string keyWord,string inputString)
        {
            if(!_fastSearch.ContainsKey(keyWord))
            _fastSearch.Add(keyWord, inputString);
        }
        public string Get(string keyWord)
        {
            return _fastSearch[keyWord];
        }
    }
}
