﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    public class IntStack : IEnumerable<int>
    {
        public IntStack()
        {
            _innerStack = new List<int>();
        }
        private List<int> _innerStack;
        public int Count
        {
            get
            {
                return _innerStack.Count;
            }
        }
        public int Pop()
        {

            int itm = _innerStack[Count - 1];
            _innerStack.RemoveAt(Count - 1);
            return itm;
        }
        public int Peek()
        {
           return _innerStack[Count - 1];
        }
        public void Push(int itm)
        {
            _innerStack.Add(itm);
        }
        public void Clear()
        {
            _innerStack.Clear();
        }

        IEnumerator<int> IEnumerable<int>.GetEnumerator()
        {
            return _innerStack.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _innerStack.GetEnumerator();
        }
    }
}
