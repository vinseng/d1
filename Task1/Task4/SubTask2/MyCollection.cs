﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
namespace Task4_Collections
{
    public class MyCollection<T> : ICollection<T> where T : IEquatable<T>
    {
        public MyCollection()
        {
            _innerCollection = new List<T>();
        }
        #region somedefinitions
        private List<T> _innerCollection;
        public int Count
        {
            get
            {
                return _innerCollection.Count;
            }
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _innerCollection.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _innerCollection.GetEnumerator();
        }
        public T this[int index]
        {
            get { return (T)_innerCollection[index]; }
            set { _innerCollection[index] = value; }
        }
        public bool Contains(T item)
        {
            if (item == null) return false;
            


            foreach (T itm in _innerCollection)
            {

                if (itm.Equals(item))
                {
                    return true;
                }
            }

            return false;
        }
        public void Add(T item)
        {

            if (!Contains(item))
            {
                _innerCollection.Add(item);
            }

        }
        public void Clear()
        {
            _innerCollection.Clear();
        }
        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {

            return _innerCollection.Remove(item);

        }
        public void CopyTo(T[] array, int arrayIndex)
        {

            _innerCollection.CopyTo(array, arrayIndex);
        }
        #endregion

        
    }
}
