﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task4_Collections.SubTask3;

using Task4_Collections;
namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            #region subtask1
            TextStore ts = new TextStore();
            ts.Add("test");
            Console.WriteLine(ts.Search("test"));
            Console.ReadKey();
            #endregion
            #region subtask2
            //MyCollection<int> mc = new MyCollection<int>();
            //mc.Add(1);
            //mc.Add(1000);
            //Console.WriteLine(mc.Contains(1));
            //Console.ReadKey();
            #endregion
            #region subtask3
            //List<int> inputList = new List<int>() { 1, 2, -3 };
            //IntList il = new IntList(inputList);

            //foreach (int item in il)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.ReadKey();

            #endregion

        }
    }
}
