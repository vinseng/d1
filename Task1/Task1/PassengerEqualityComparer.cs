﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task1
{
    public class PassengerEqualityComparer: IEqualityComparer<Passenger>
    {
        public  bool Equals(Passenger obj1,Passenger obj2)
        {
            if(obj1!=null&&obj2!=null)
            {
                return obj1.Equals(obj2);
            }
            else
            {
                return false;
            }
            
        }
        public int GetHashCode(Passenger obj)
        {
            return obj.GetHashCode();
        }
    }
}
