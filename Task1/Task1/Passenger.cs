﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task1
{
    public class Passenger:IComparable<Passenger>, IEquatable<Passenger>
    {
        public Passenger(string name,string passId,string tickedId,int placeNumber)
        {
            Name = name;
            PassId = passId;
            TicketId = tickedId;
            PlaceNumber=placeNumber;
        }
        private string _name;
        private string _passId;
        private string _ticketId;
        private int _placeNumber;
        public string Name
        {
            get
            {
                return _name;
            }
            private set
            {
                _name = value;
            }
        }
        public string PassId
        {
            get
            {
                return _passId;
            }
            private set
            {
                _passId = value;
            }
        }
        public string TicketId
        {
            get
            {
                return _ticketId;
            }
            private set
            {
                _ticketId = value;
            }
        }

        public int PlaceNumber
        {
            get
            {
                return _placeNumber;
            }
            private set
            {
                _placeNumber = value;
            }
        }
        public bool Equals(Passenger p)
        {

           if(p==null)
            {
                return false;
            }
            if (System.Object.ReferenceEquals(this, p))
            {
                return true;
            }
            else
            {
                return (this.Name == p.Name && this.PassId == p.PassId && this.TicketId == p.TicketId && this.PlaceNumber == p.PlaceNumber);
            }

        }
        public override bool Equals(object obj)
        {
            return Equals(obj as Passenger);
           
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ PassId.GetHashCode() ^ TicketId.GetHashCode()^ PlaceNumber;
        }
        public int CompareTo(Passenger obj)
        {
            if (obj == null)
            {
                return -1;
            }
            else
            {

                if (this.Equals(obj))
                {
                    return 0;
                }
                else
                {
                    return String.Compare(this.Name, obj.Name);
                }
            }
        }
    }
}
