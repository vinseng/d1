﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9_Caching
{
    public class FibonacciManager
    {
        public FibonacciManager(ICacheProvider cacheProvider)
        {
            _cacheProvider = cacheProvider;
        }
        private ICacheProvider _cacheProvider;
        public int GetFibonacci(int index)
        {
            int a=0;
            int b = 1;
            if (_cacheProvider.TryGet<int>(index.ToString(),out a))
            {
                return a;
            }
            for(int i=0;i<index;i++)
            {
                int temp = a;
                a = b;
                b = temp + b;
            }
            _cacheProvider.Set<int>(index.ToString(), a,null);
            return a;
        }
    }
}
