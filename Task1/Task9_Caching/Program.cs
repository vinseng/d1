﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CachingSolutionsSamples; 
namespace Task9_Caching
{
    class Program
    {
        static void Main(string[] args)
        {
            ICacheProvider mcp = null;
            Console.WriteLine("Select provider:1-MemoryCache,2-Redis (You must have an available server)");
            string provider = Console.ReadLine();
            try
            {
                switch (provider)
            {
                case "1":
                    mcp = new MyMemoryCacheProvider();
                    break;
                case "2":
                    mcp = new MyRedisCacheProvider();
                    break;
                default:
                    mcp = new MyMemoryCacheProvider();
                    break;

            }
            string task = String.Empty;
            do
            {

                Console.WriteLine("Select:1-Get DB task,2-fibonacci,@Exit-for exit");
                task = Console.ReadLine();
                switch (task)
                {
                    case "1":
                        DbTask(mcp);
                        break;
                    case "2":
                        FibonacciTask(mcp);
                        break;
                    default:

                        break;

                }
            }
            while (task != "@Exit");
            }
            catch (StackExchange.Redis.RedisConnectionException ex)
            {
                Console.WriteLine("You must have an available Redis server!" + System.Environment.NewLine + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadKey();
        }
        private static void DbTask(ICacheProvider mcp)
        {
            
                Console.WriteLine("DB Task!");
                DbElementManager dbEm = new DbElementManager(mcp);
                dbEm.GetCustomers();
                var r = dbEm.GetCustomers();
                Console.WriteLine("Count of customers:" + r.Count());
            
        }
        private static void FibonacciTask(ICacheProvider mcp)
        {
            Console.WriteLine("Fibonacci Task!");
            FibonacciManager fbm = new FibonacciManager(mcp);
            Console.WriteLine("Enter the item number in the Fibonacci sequence");
            string index = Console.ReadLine();
            Console.WriteLine("Item="+fbm.GetFibonacci(Convert.ToInt32(index)));
            
        }
    }
}
