﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CachingSolutionsSamples;
using System.Threading;
using NorthwindLibrary;

namespace Task9_Caching
{
    public class DbElementManager
    {
        public DbElementManager(ICacheProvider cacheProvider)
        {
            _cacheProvider = cacheProvider;
        }
        private ICacheProvider _cacheProvider;
        public IEnumerable<Category> GetCategories()
        {
            Console.WriteLine("Get Categories");
            string message = "From Cache";
            var prefix = "Cache_Categories";
            IEnumerable<Category> categories;

            if (!_cacheProvider.TryGet<IEnumerable<Category>>(prefix, out categories))
            {


                message = "From DB"; 

                    using (var dbContext = new Northwind())
                    {
                        dbContext.Configuration.LazyLoadingEnabled = false;
                        dbContext.Configuration.ProxyCreationEnabled = false;
                        categories = dbContext.Categories.ToList();
                        _cacheProvider.Set(prefix, categories,null);
                    }
                
            }
            Console.WriteLine(message);
            return categories;
        }
        public IEnumerable<Customer> GetCustomers()
        {
            Console.WriteLine("Get Customers");
            string message = "From Cache";
            var prefix = "Cache_Customers";
            IEnumerable<Customer> categories;

            if (!_cacheProvider.TryGet<IEnumerable<Customer>>(prefix, out categories))
            {


                message = "From DB";

                using (var dbContext = new Northwind())
                {
                    dbContext.Configuration.LazyLoadingEnabled = false;
                    dbContext.Configuration.ProxyCreationEnabled = false;
                    categories = dbContext.Customers.ToList();
                    _cacheProvider.Set(prefix, categories,1);
                }

            }
            Console.WriteLine(message);
            return categories;
        }
    }
}
