﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task9_Caching
{
    public interface ICacheProvider
    {
        bool TryGet<T>(string key, out T value);
       
        void Set<T>(string key, T value, int? duration);
        void Clear(string key);
        
    }
}
