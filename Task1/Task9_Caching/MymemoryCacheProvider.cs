﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;
using System.Threading;

namespace Task9_Caching
{
    public class MyMemoryCacheProvider:ICacheProvider
    {
        public MyMemoryCacheProvider()
        {
            _cache = MemoryCache.Default;
        }
        private ObjectCache _cache;
       
        public  bool TryGet<T>(string key, out T value)
        {

            if(_cache.Get(key)==null)
            {
                value = default(T);
                
            }
            else
            {
                value= (T)_cache.Get(key);
                return true;
            }
            return false;
        }
        
        public void Set<T>(string prefix, T value, int? duration)
        {
            var user = Thread.CurrentPrincipal.Identity.Name;
           
            _cache.Set(prefix + user, value, duration.HasValue ? DateTime.Now.AddMinutes(duration.Value) : ObjectCache.InfiniteAbsoluteExpiration);
        }
        public void Clear(string key)
        {

            _cache.Remove(key);
        }
      
    }
}
