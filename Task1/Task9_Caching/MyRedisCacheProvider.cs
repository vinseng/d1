﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NorthwindLibrary;
using StackExchange.Redis;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Threading;

namespace Task9_Caching
{
    public class MyRedisCacheProvider : ICacheProvider
    {
        public MyRedisCacheProvider()
        {
            redisConnection = ConnectionMultiplexer.Connect("localhost");
           
        }
        public MyRedisCacheProvider(string hostName)
        {
            redisConnection = ConnectionMultiplexer.Connect(hostName);
        }
        private ConnectionMultiplexer redisConnection;
       
        public bool TryGet<T>(string key, out T value)
        {
            var db = redisConnection.GetDatabase();
            byte[] s = db.StringGet(key);
            
            if (s == null)
            {
                value = default(T);

            }
            else
            {
                DataContractSerializer serializer = new DataContractSerializer(
            typeof(T));
                using (var stream = new MemoryStream(s))
                {
                    value = (T)serializer
                .ReadObject(stream);
                }
                return true;
            }
            return false;
        }
       
        public void Set<T>(string prefix, T value, int? duration)
        {
            var user = Thread.CurrentPrincipal.Identity.Name;
            var db = redisConnection.GetDatabase();
            var key = prefix + user;

            if (value == null)
            {
                db.StringSet(key, RedisValue.Null);
            }
            else
            {
                DataContractSerializer serializer = new DataContractSerializer(
            typeof(T));
                using (var stream = new MemoryStream())
                {
                    serializer.WriteObject(stream, value);
                    if (duration.HasValue)
                    {
                        db.StringSet(key, stream.ToArray(), TimeSpan.FromMinutes((int)duration));
                    }
                    else
                    {
                        db.StringSet(key, stream.ToArray());
                    }
                }
            }
        }
        public void Clear(string key)
        {
            var db = redisConnection.GetDatabase();
            db.KeyDelete(key);
        }
        
    }
}
