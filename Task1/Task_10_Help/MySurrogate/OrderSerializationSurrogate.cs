﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Task.DB;

namespace Task.MySurrogate
{
    public class OrderSerializationSurrogate : ISerializationSurrogate
    {
        
        public OrderSerializationSurrogate(DbContext context)
        {
            _context = context;
        }
        private DbContext _context;
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            
            var ord = (Order_Detail)obj;
            var t = (_context as IObjectContextAdapter).ObjectContext;
            t.LoadProperty(ord, f => f.Order);
            t.LoadProperty(ord, f => f.Product);
            info.AddValue("OrderID", ord.OrderID);
            info.AddValue("ProductID", ord.ProductID);
            info.AddValue("UnitPrice", ord.UnitPrice);
            info.AddValue("Quantity", ord.Quantity);
            info.AddValue("Discount", ord.Discount);
            info.AddValue("Order", ord.Order);
            info.AddValue("Product", ord.Product);
            
        }
        //List<Order_Detail> ordList = new List<Order_Detail>();
        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            
            var ord = (Order_Detail)obj;
            ord.OrderID = info.GetInt32("OrderID");
            ord.ProductID = info.GetInt32("ProductID");
            ord.UnitPrice = info.GetDecimal("UnitPrice");
            ord.Quantity = (short)info.GetInt32("Quantity");
            ord.Discount = (float)info.GetDecimal("Discount") ;
            ord.Order = info.GetValue("Order", typeof(Order)) as Order;
            ord.Product = info.GetValue("Product", typeof(Product)) as Product;
            
            return ord;
        }
    }
}
