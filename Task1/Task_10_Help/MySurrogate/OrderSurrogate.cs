﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Task.DB;
using System.Reflection;
using System.Collections.ObjectModel;
using System.CodeDom;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Task.MySurrogate
{
    public class OrderSurrogate : IDataContractSurrogate
    {
        public  OrderSurrogate(DbContext context)
        {
            _context = context;
        }
        private DbContext _context;
        
        public Type GetDataContractType(Type type)
        {
            if (type == typeof(IEnumerable<Order>))
                return typeof(IEnumerable<OrderSurrogated>);
            
            return type;
        }

        public object GetObjectToSerialize(object obj, Type targetType)
        {
           IEnumerable<Order> ords = obj as IEnumerable<Order>;
            if (ords != null)
            {
                var os = new List<OrderSurrogated>();
                OrderSurrogated result = null;
                foreach (var ord in ords)
                {
                    if (ord.Order_Details != null && !ord.Order_Details.Any())
                    {
                        if (_context is IObjectContextAdapter)
                        {
                            var dbContext = (_context as IObjectContextAdapter).ObjectContext;
                            dbContext.LoadProperty(ord, f => f.Order_Details);
                        }

                    }
                    result = new OrderSurrogated()
                    {
                        OrderID = ord.OrderID,
                        CustomerID = ord.CustomerID,
                        EmployeeID = ord.EmployeeID,
                        OrderDate = ord.OrderDate,
                        RequiredDate = ord.RequiredDate,
                        ShippedDate = ord.ShippedDate,
                        ShipVia = ord.ShipVia,
                        Freight = ord.Freight,
                        ShipName = ord.ShipName,
                        ShipAddress = ord.ShipAddress,
                        ShipCity = ord.ShipCity,
                        ShipRegion = ord.ShipRegion,
                        ShipPostalCode = ord.ShipPostalCode,
                        ShipCountry = ord.ShipCountry,
                        Customer = ord.Customer,
                        Employee = ord.Employee,
                        Order_Details = ord.Order_Details,
                        Shipper = ord.Shipper
                    };
                    os.Add(result);
                }
               
                return os ;
            }
            return obj;
        }
        public object GetDeserializedObject(object obj, Type targetType)
        {
            //OrderSurrogated ord = obj as OrderSurrogated;
            IEnumerable<OrderSurrogated>  ords =obj as IEnumerable<OrderSurrogated>;
            if (ords != null)
            {

                List<Order> os = new List<Order>();
                
                foreach (var ord in ords)
                {
                    os.Add(new Order()
                    {
                        OrderID = ord.OrderID,
                        CustomerID = ord.CustomerID,
                        EmployeeID = ord.EmployeeID,
                        OrderDate = ord.OrderDate,
                        RequiredDate = ord.RequiredDate,
                        ShippedDate = ord.ShippedDate,
                        ShipVia = ord.ShipVia,
                        Freight = ord.Freight,
                        ShipName = ord.ShipName,
                        ShipAddress = ord.ShipAddress,
                        ShipCity = ord.ShipCity,
                        ShipRegion = ord.ShipRegion,
                        ShipPostalCode = ord.ShipPostalCode,
                        ShipCountry = ord.ShipCountry,
                        Customer = ord.Customer,
                        Employee = ord.Employee,
                        Order_Details = ord.Order_Details,
                        Shipper = ord.Shipper
                    });
                    
                }
                return os;

            }
            return obj;
        }
        public object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType)
        {
          
            throw new NotImplementedException();
        }
        public object GetCustomDataToExport(Type memberInfo, Type dataContractType)
        {
            throw new NotImplementedException();
        }
        public void GetKnownCustomDataTypes(Collection<Type> customDataTypes)
        {
            throw new NotImplementedException();
        }
        public Type GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData)
        {
            throw new NotImplementedException();
        }
        public CodeTypeDeclaration ProcessImportedType(CodeTypeDeclaration typeDeclaration, CodeCompileUnit compileUnit)
        {
            
            throw new NotImplementedException();
        }
    }
}
