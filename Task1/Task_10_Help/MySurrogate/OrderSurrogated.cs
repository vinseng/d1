﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Task.DB;

namespace Task.MySurrogate
{
    [DataContract]
    public class OrderSurrogated
    {
        [DataMember]
        public int OrderID { get; set; }

        [DataMember]
        public string CustomerID { get; set; }
       [DataMember]
        public int? EmployeeID { get; set; }
        [DataMember]
        public DateTime? OrderDate { get; set; }
        [DataMember]
        public DateTime? RequiredDate { get; set; }
       [DataMember]
        public DateTime? ShippedDate { get; set; }
        [DataMember]
        public int? ShipVia { get; set; }
        [DataMember]

        public decimal? Freight { get; set; }
        [DataMember]

        public string ShipName { get; set; }

        [DataMember]
        public string ShipAddress { get; set; }

        [DataMember]
        public string ShipCity { get; set; }

        [DataMember]
        public string ShipRegion { get; set; }

       [DataMember]
        public string ShipPostalCode { get; set; }

        [DataMember]
        public string ShipCountry { get; set; }
        [DataMember]
        public  Customer Customer { get; set; }
        [DataMember]
        public  Employee Employee { get; set; }

       // [CollectionDataContract(Name = "Order_Details",ItemName = "Order_Detail")]
        [DataMember]
        public  ICollection<Order_Detail> Order_Details { get; set; }
        [DataMember]
        public  Shipper Shipper { get; set; }
    }
}
