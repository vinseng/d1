namespace Task.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;
    [Serializable]
    [KnownType(typeof(Category)), KnownType(typeof(Supplier)), KnownType(typeof(Order_Detail))]
    public partial class Product: ISerializable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            Order_Details = new HashSet<Order_Detail>();
        }

        public int ProductID { get; set; }

        [Required]
        [StringLength(40)]
        public string ProductName { get; set; }

        public int? SupplierID { get; set; }

        public int? CategoryID { get; set; }

        [StringLength(20)]
        public string QuantityPerUnit { get; set; }

        [Column(TypeName = "money")]
        public decimal? UnitPrice { get; set; }

        public short? UnitsInStock { get; set; }

        public short? UnitsOnOrder { get; set; }

        public short? ReorderLevel { get; set; }

        public bool Discontinued { get; set; }

        public virtual Category Category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order_Detail> Order_Details { get; set; }

        public virtual Supplier Supplier { get; set; }
        private Product(SerializationInfo information, StreamingContext context)
        {
            
            ProductID = information.GetInt32("ProductID");
            ProductName= information.GetString("ProductName");
            SupplierID= information.GetValue("SupplierID",typeof(int?)) as int?;
            CategoryID= information.GetValue("CategoryID", typeof(int?)) as int?;
            QuantityPerUnit= information.GetString("QuantityPerUnit");
            UnitPrice= information.GetValue("UnitPrice",typeof(decimal?))  as decimal?;
            UnitsInStock= information.GetValue("UnitsInStock", typeof(short?)) as short?;
            UnitsOnOrder= information.GetValue("UnitsOnOrder", typeof(short?)) as short?;
            ReorderLevel= information.GetValue("ReorderLevel", typeof(short?)) as short?;
            Discontinued= information.GetBoolean("Discontinued");
            Category= information.GetValue("Category", typeof(Category)) as Category;
            Order_Details= information.GetValue("Order_Details", typeof(ICollection<Order_Detail>)) as ICollection<Order_Detail>;
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (context.Context is IObjectContextAdapter)
            {
                var t = (context.Context as IObjectContextAdapter).ObjectContext;
                t.LoadProperty(this, f => f.Category);
                t.LoadProperty(this, f => f.Order_Details);
                t.LoadProperty(this, f => f.Supplier);
            }
            info.AddValue("ProductID", ProductID);
            info.AddValue("ProductName", ProductName);
            info.AddValue("SupplierID", SupplierID);
            info.AddValue("CategoryID", CategoryID);
            info.AddValue("QuantityPerUnit", QuantityPerUnit);
            info.AddValue("UnitPrice", UnitPrice);
            info.AddValue("UnitsInStock", UnitsInStock);
            info.AddValue("UnitsOnOrder", UnitsOnOrder);
            info.AddValue("ReorderLevel", ReorderLevel);
            info.AddValue("Discontinued", Discontinued);
            info.AddValue("Category", Category);
            info.AddValue("Order_Details", Order_Details);
            info.AddValue("Supplier", Supplier);
        }
    }
}
