﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task.DB;
using Task.TestHelpers;
using System.Runtime.Serialization;
using System.Linq;
using System.Collections.Generic;
using Task.MySurrogate;
using System.Runtime.Serialization.Formatters;

namespace Task
{
	[TestClass]
	public class SerializationSolutions
	{
		Northwind dbContext;

		[TestInitialize]
		public void Initialize()
		{
			dbContext = new Northwind();
		}

		[TestMethod]
		public void SerializationCallbacks()
		{
			dbContext.Configuration.ProxyCreationEnabled = false;
            StreamingContext sc = new StreamingContext(StreamingContextStates.Persistence, dbContext);
            var tester = new XmlDataContractSerializerTester<IEnumerable<Category>>(new NetDataContractSerializer(sc), true);
			var categories = dbContext.Categories.ToList();

			var c = categories.First();

			tester.SerializeAndDeserialize(categories);
		}

		[TestMethod]
		public void ISerializable()
		{
			dbContext.Configuration.ProxyCreationEnabled = false;
            //dbContext.Configuration.LazyLoadingEnabled = true;
            StreamingContext sc = new StreamingContext(StreamingContextStates.Persistence, dbContext);
            var tester = new XmlDataContractSerializerTester<IEnumerable<Product>>(new NetDataContractSerializer(sc), true);
			var products = dbContext.Products.ToList();

			tester.SerializeAndDeserialize(products);
		}


		[TestMethod]
		public void ISerializationSurrogate()
		{

            var selector = new SurrogateSelector();
            selector.AddSurrogate(
                typeof(Order_Detail),
                new StreamingContext(StreamingContextStates.Persistence, null),
                new OrderSerializationSurrogate(dbContext));
            NetDataContractSerializer netDataContractSerializer = 
                new NetDataContractSerializer(new StreamingContext(StreamingContextStates.Persistence, null),
                int.MaxValue,true,FormatterAssemblyStyle.Simple,selector);
            
            dbContext.Configuration.ProxyCreationEnabled = false;
            dbContext.Configuration.LazyLoadingEnabled = false;
            var tester = new XmlDataContractSerializerTester<IEnumerable<Order_Detail>>(netDataContractSerializer, true);
			var orderDetails = dbContext.Order_Details.ToList();

			tester.SerializeAndDeserialize(orderDetails);
            

        }

		[TestMethod]
		public void IDataContractSurrogate()
		{
            //dbContext.Configuration.ProxyCreationEnabled = true;
            dbContext.Configuration.ProxyCreationEnabled = false;
			dbContext.Configuration.LazyLoadingEnabled = false;

            //var tester = new XmlDataContractSerializerTester<IEnumerable<Order>>(new DataContractSerializer(typeof(IEnumerable<Order>)), true);
            var tester = new XmlDataContractSerializerTester<IEnumerable<Order>>(new DataContractSerializer(typeof(IEnumerable<Order>), new DataContractSerializerSettings
            {
                DataContractSurrogate = new OrderSurrogate(dbContext),
                PreserveObjectReferences = true
            }), true);
            var orders = dbContext.Orders.ToList();

			tester.SerializeAndDeserialize(orders);
		}
	}
}
