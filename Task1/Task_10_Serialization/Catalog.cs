﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
namespace Task_10_Serialization
{
    [Serializable]
    [XmlRoot(ElementName ="catalog",Namespace = "http://library.by/catalog")]
    public class Catalog
    {
        public Catalog()
        {
            this.CatalogList = new List<Book>();
            Date = DateTime.Now.Date;
        }
        [XmlAttribute(DataType = "date",AttributeName ="date")]
        public DateTime Date;
        [XmlElement(ElementName ="book")]
        public List<Book> CatalogList { get; set; }
    }
}
