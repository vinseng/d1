﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Task;
namespace Task_10_Serialization
{
    class Program
    {
        private const string InputFileName = "books.xml";
        private const string OutFileName = "books_out.xml";
        static void Main(string[] args)
        {

            string task = String.Empty;
            do
            {

                Console.WriteLine("Select:1-Basic serialization,2-Custom Serialization,@Exit-for exit");
                task = Console.ReadLine();
                switch (task)
                {
                    case "1":
                        BasicSerialization();
                        break;
                    case "2":
                        CustomSerialization();
                        break;
                    default:

                        break;

                }
            }
            while (task != "@Exit");
            
            
            Console.ReadKey();
        }
        private static void BasicSerialization()
        {
            ISerializer serializer = new MyXmlSerializer();
            Catalog cat = serializer.Deserialize<Catalog>(InputFileName);
            Console.WriteLine("Object Desialized");
            serializer.Serialize<Catalog>(OutFileName, cat);
            Console.WriteLine("Object Serialized");
        }
        private static void CustomSerialization()
        {
            SerializationSolutions ss = new SerializationSolutions();
            ss.Initialize();
            Console.WriteLine("Select:1-SerializationCallbacks,2-ISerializable,3-ISerializationSurrogate,4-IDataContractSurrogate");
            string task = Console.ReadLine();
            switch (task)
            {
                case "1":
                    ss.SerializationCallbacks();
                    break;
                case "2":
                    ss.ISerializable();
                    break;
                case "3":
                    ss.ISerializationSurrogate();
                    break;
                case "4":
                    ss.IDataContractSurrogate();
                    break;
                default:

                    break;

            }
        }
    }
}
