﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
namespace Task_10_Serialization
{
    [Serializable]
    [XmlRoot("book")]
    public class Book
    {
        [XmlAttribute(AttributeName ="id")]
        public string Id;
        [XmlElement("isbn")]
        [OptionalField(VersionAdded =2)]
        public string Isbn;
        [XmlElement("author")]
        public string Author;
        [XmlElement("title")]
        public string Title;
        [XmlElement("genre")]
        public GenreEnum Genre;
        [XmlElement("publisher")]
        public string Publisher;
        [XmlElement(DataType = "date", ElementName = "publish_date")]
        public DateTime PublishDate;
        [XmlElement("description")]
        public string Description; 
        [XmlElement(DataType ="date",ElementName ="registration_date")]
        public DateTime RegistrationDate;
    }
}
