﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
namespace Task_10_Serialization
{
    public class MyXmlSerializer:ISerializer
    {
        public void Serialize<T>(string outFile, object instance) where T : class, new()
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new FileStream(outFile, FileMode.Create))
            {
                serializer.Serialize(stream, instance);
                stream.Close();
            }
        }
       public  T Deserialize<T>(string inputFile) where T:class,new()
        {
            var result=default(T);
            var serializer = new XmlSerializer(typeof(T));

            using (var fs = new FileStream(inputFile, FileMode.Open))
            {
                result=serializer.Deserialize(fs) as T;
            }
            return result as T;
        }
    }
}
