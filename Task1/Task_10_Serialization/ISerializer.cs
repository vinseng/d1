﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10_Serialization
{
    public interface ISerializer
    {
        void Serialize<T>(string outFile,object instance) where T : class, new();
        T Deserialize<T>(string inputFile) where T:class,new();
    }
}
