﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
namespace Task_10_Serialization
{
    [Serializable]
    public enum  GenreEnum
    {
        [XmlEnum(Name = "Computer")]
        Computer,
        [XmlEnum(Name = "Science Fiction")]
        ScienceFiction,
        [XmlEnum(Name = "Horror")]
        Horror,
        [XmlEnum(Name = "Romance")]
        Romance,
        [XmlEnum(Name = "Fantasy")]
        Fantasy
        
    }
}
