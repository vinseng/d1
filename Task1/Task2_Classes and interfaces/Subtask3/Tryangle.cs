﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_ClassesAndInterfaces
{
    public class Triangle : IFigure
    {
        public Triangle(double sideA, double sideB, double sideC)
        {
            _sideA = sideA;
            _sideB = sideB;
            _sideC = sideC;
        }
        private double _sideA;
        private double _sideB;
        private double _sideC;
        public FiguresType GetFigureType()
        {
            return FiguresType.Triangle;
        }
        public double GetArea()
        {
            double p = (_sideA + _sideB + _sideC) / 2;
            return Math.Sqrt(p*(p-_sideA)*(_sideB) *(_sideC));
        }
    }
}
