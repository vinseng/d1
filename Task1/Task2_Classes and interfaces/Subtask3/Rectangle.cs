﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_ClassesAndInterfaces
{
    public class Rectangle : IFigure
    {
        public Rectangle(double sideA,double sideB)
        {
            _sideA = sideA;
            _sideB = sideB;
        }
        private double _sideA;
        private double _sideB;
        public FiguresType GetFigureType()
        {
            return FiguresType.Rectangle;
        }
        public double GetArea()
        {
            return _sideB* _sideA;
        }
    }
}
