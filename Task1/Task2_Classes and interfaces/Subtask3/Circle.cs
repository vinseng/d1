﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_ClassesAndInterfaces
{
    public class Circle : IFigure
    {
        public Circle(double radius)
        {
            _radius = radius;
        }
        private double _radius;
        public FiguresType GetFigureType()
        {
            return FiguresType.Circle;
        }
        public double GetArea()
        {
            return Math.PI*_radius*_radius;
        }
    }
}
