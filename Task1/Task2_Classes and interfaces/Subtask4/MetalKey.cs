﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Task2_ClassesAndInterfaces
{
    public class MetalKey : IKey
    {
        
        public void OpenDoorProcess()
        {
            Console.WriteLine("Проверните ключ в замке");
        }
        public KeysType GetKeyType()
        {
            return KeysType.MetalKey;
        }
    }
}
