﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_ClassesAndInterfaces
{
    public interface IKey
    {
        void OpenDoorProcess();
        KeysType GetKeyType();
    }
}
