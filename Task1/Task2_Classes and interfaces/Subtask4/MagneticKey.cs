﻿using System;
using System.Collections.Generic;
using System.Text;
namespace Task2_ClassesAndInterfaces
{
    public class MagneticKey : IKey
    {
        
        public void OpenDoorProcess()
        {
            Console.WriteLine("Проведите карточкой по замку");
        }
        public KeysType GetKeyType()
        {
            return KeysType.MagneticKey;
        }
    }
}
