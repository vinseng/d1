﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_ClassesAndInterfaces
{
    public class ProximityKey :  IKey
    {
        
        public void OpenDoorProcess()
        {
            Console.WriteLine("Нажмите на кнопку, чтобы открыть дверь");
        }
        public KeysType GetKeyType()
        {
            return KeysType.ProximityKey;
        }
    }
}
