﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_ClassesAndInterfaces
{
    public class DescendantsNumber
    {
        // Напишите класс, который умеет хранить информацию об общем количестве созданных экземпляров своего типа.
        public DescendantsNumber()
        {
            DescedantsCounter++;
        }
        private static int DescedantsCounter=0;
        public static int GetDescedantsCount()
        {
            return DescedantsCounter;
        }
    }
}
