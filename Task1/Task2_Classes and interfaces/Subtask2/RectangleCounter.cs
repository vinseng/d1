﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_ClassesAndInterfaces
{//Напишите класс, который по заданной площади поверхности рассчитает количество прямоугольников 
 //указанного размера, на которые можно разбить указанную площадь.Размер прямоугольников фиксированный.
    public static class RectangleNumber
    {
       
        
       /// <summary>
       /// 
       /// </summary>
       /// <param name="area">Площадь</param>
       /// <param name="sideA">Сторона А</param>
       /// <param name="sideB">Сторона B</param>
       /// <returns></returns>
        public static Int64 GetRectangleNumber(double area,double sideA,double sideB)
        {
            return (Int64)Math.Truncate(area/(sideA*sideB));
        }
    }
}
