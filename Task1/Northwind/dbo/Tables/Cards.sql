﻿CREATE TABLE [dbo].[Cards] (
	[CardID] int IDENTITY (1, 1) NOT NULL PRIMARY KEY  CLUSTERED ,
	"CardNumber" INT NOT NULL,
	 "Card Holder" nvarchar (30) NOT NULL,
	 "EmployeeID" "int" NOT NULL ,
CONSTRAINT "FK_Cards_Employees" FOREIGN KEY (EmployeeID)
REFERENCES "Employees" ("EmployeeID")
	)
