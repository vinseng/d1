﻿--Subtask 1.3.1
SELECT od.OrderID 
FROM [Order Details]  od
WHERE od.Quantity BETWEEN 3 AND 10
--Subtask 1.3.2
SELECT c.CustomerID , c.Country 
FROM Customers c
Where LEFT(c.Country,1) BETWEEN 'b' AND 'g'
ORDER BY c.Country 
--Subtask 1.3.3
SELECT c.CustomerID , c.Country 
FROM Customers c
Where LEFT(c.Country,1) >= 'b' AND LEFT(c.Country,1)<= 'g'
ORDER BY c.Country 
 