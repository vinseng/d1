﻿
--Task 2.3.1
SELECT e.*
FROM
Region r 
JOIN Territories t ON r.RegionID=t.RegionID
JOIN EmployeeTerritories et On et.TerritoryID=t.TerritoryID
JOIN Employees e On e.EmployeeID=et.EmployeeID
WHERE r.RegionID='2'
--Task 2.3.2
SELECT c.CompanyName,COUNT(OrderID)
FROM
Customers c
LEFT JOIN ORDERS o ON o.CustomerID=c.CustomerID
GROUP BY c.CompanyName
ORDER BY COUNT(OrderID)