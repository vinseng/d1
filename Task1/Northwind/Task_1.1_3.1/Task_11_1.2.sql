﻿--Subtask 1.2.1
SELECT c.CompanyName,c.Country 
FROM Customers c
Where c.Country IN ('USA','Canada')
ORDER BY c.CompanyName,c.Country

--Subtask 1.2.2
SELECT c.CompanyName,c.Country 
FROM Customers c
Where c.Country NOT IN ('USA','Canada')
ORDER BY c.CompanyName
--Subtask 1.3.3
SELECT DISTINCT c.Country 
FROM Customers c

ORDER BY c.Country  desc