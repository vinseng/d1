﻿--SubTask 1.1
SELECT o.OrderID , o.ShippedDate,  o.ShipVia
FROM Orders o
WHERE o.ShippedDate>='1998-05-06' AND o.ShipVia>=2

--SubTask 1.2

SELECT  
CASE WHEN o.ShippedDate IS NULL THEN 'Not Shipped'  END AS ShippedDate,
o.OrderID
FROM Orders o
WHERE o.ShippedDate IS NULL

--SubTask 1.3

SELECT 
 o.OrderID AS "Order Number",
CASE WHEN o.ShippedDate IS NULL THEN 'Not Shipped' ELSE CAST(o.ShippedDate AS nvarchar(15))  END AS "Shipped Date",
o.OrderID
FROM Orders o
WHERE   o.ShippedDate>'1998-05-06' OR o.ShippedDate IS NULL