﻿
--1.0->1.1
if NOT exists (select * from sysobjects where id = object_id('dbo.Cards'))
BEGIN
CREATE TABLE "Cards" (
	"CardID" "int" IDENTITY (1, 1) NOT NULL PRIMARY KEY  CLUSTERED ,
	"CardNumber" INT NOT NULL,
	 "Card Holder" nvarchar (30) NOT NULL,
	 "EmployeeID" "int" NOT NULL ,
CONSTRAINT "FK_Cards_Employees" FOREIGN KEY (EmployeeID)
REFERENCES "Employees" ("EmployeeID")
	)
END
--1.1->1.3
if  exists (select * from sysobjects where id = object_id('dbo.Region')) AND
NOT exists (select * from sysobjects where id = object_id('dbo.Regions'))
BEGIN
	exec sp_rename 'Region','Regions'

END
if  exists (select * from sysobjects where id = object_id('dbo.Customers'))
AND NOT exists(select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='Customers' And COLUMN_NAME='DateOfFoundation')
BEGIN
ALTER TABLE dbo.Customers ADD  "DateOfFoundation" DATETIME
END