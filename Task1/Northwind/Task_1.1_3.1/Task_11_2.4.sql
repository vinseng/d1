﻿--Task 2.4.1
SELECT s.CompanyName 
FROM Suppliers s
WHERE EXISTS(select p.UnitsInStock FROM Products p where p.UnitsInStock=0 AND p.SupplierID=s.SupplierID )
--Task 2.4.2
SELECT e.FirstName+e.LastName 
FROM Employees e
WHERE (SELECT COUNT(*) FROM ORDERS o where o.EmployeeID=e.EmployeeID)>150
--Task 2.4.3
SELECT c.CompanyName FROM
Customers c 
Where NOT EXISTS(SELECT 1 FROM ORDERS o where c.CustomerID=o.CustomerID)