﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Reflection;
namespace Task7_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            _cont = new Task7_Test.Container(new List<IFilter<Type>>() { new ClassFilter(),new PropertyFilter(),new FieldFilter()});
            _cont.AddAssembly(Assembly.GetExecutingAssembly());
        }
        private Container _cont;

        private void btLoadAssembly_Click(object sender, EventArgs e)
        {
            _cont.AddAssembly(Assembly.GetExecutingAssembly());
        }

        private void btLoadCustomerBLL_Click(object sender, EventArgs e)
        {
           
            var  res= _cont.CreateInstance(typeof(CustomerBLL));
            if (res!=null)
            {
                tbResult.Text = ((CustomerBLL)res).logger.GetLog();
            }
        }

        private void btLoadCustomerBLL2_Click(object sender, EventArgs e)
        {
           
            var res = _cont.CreateInstance(typeof(CustomerBLL2));
            if (res != null)
            {
                tbResult.Text = ((CustomerBLL2)res).logger.GetLog();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var res = _cont.CreateInstance<CustomerBLL>();
            tbResult.Text =res.logger.GetLog();
        }

        private void btLoadCustomerBLL2Gen_Click(object sender, EventArgs e)
        {
            var res = _cont.CreateInstance<CustomerBLL2>();
            tbResult.Text = res.logger.GetLog();
        }

        private void btAddClasses_Click(object sender, EventArgs e)
        {
            _cont = new Task7_Test.Container(new List<IFilter<Type>>() { new ClassFilter(), new PropertyFilter(), new FieldFilter() });
            _cont.AddType(typeof(CustomerBLL));
            _cont.AddType(typeof(Logger));
            _cont.AddType(typeof(CustomerDAL), typeof(ICustomerDAL));

        }
    }
}
