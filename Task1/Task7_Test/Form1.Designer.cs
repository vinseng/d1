﻿namespace Task7_Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btLoadAssembly = new System.Windows.Forms.Button();
            this.btLoadCustomerBLL = new System.Windows.Forms.Button();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.btLoadCustomerBLL2 = new System.Windows.Forms.Button();
            this.btLoadCustomerBLLGen = new System.Windows.Forms.Button();
            this.LoadCustomerBLL2Gen = new System.Windows.Forms.Button();
            this.btAddClasses = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btLoadAssembly
            // 
            this.btLoadAssembly.Location = new System.Drawing.Point(500, 21);
            this.btLoadAssembly.Name = "btLoadAssembly";
            this.btLoadAssembly.Size = new System.Drawing.Size(142, 23);
            this.btLoadAssembly.TabIndex = 0;
            this.btLoadAssembly.Text = "LoadAssembly";
            this.btLoadAssembly.UseVisualStyleBackColor = true;
            this.btLoadAssembly.Click += new System.EventHandler(this.btLoadAssembly_Click);
            // 
            // btLoadCustomerBLL
            // 
            this.btLoadCustomerBLL.Location = new System.Drawing.Point(500, 67);
            this.btLoadCustomerBLL.Name = "btLoadCustomerBLL";
            this.btLoadCustomerBLL.Size = new System.Drawing.Size(142, 23);
            this.btLoadCustomerBLL.TabIndex = 1;
            this.btLoadCustomerBLL.Text = "LoadCustomerBLL";
            this.btLoadCustomerBLL.UseVisualStyleBackColor = true;
            this.btLoadCustomerBLL.Click += new System.EventHandler(this.btLoadCustomerBLL_Click);
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(23, 21);
            this.tbResult.Multiline = true;
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(445, 186);
            this.tbResult.TabIndex = 2;
            // 
            // btLoadCustomerBLL2
            // 
            this.btLoadCustomerBLL2.Location = new System.Drawing.Point(500, 108);
            this.btLoadCustomerBLL2.Name = "btLoadCustomerBLL2";
            this.btLoadCustomerBLL2.Size = new System.Drawing.Size(142, 23);
            this.btLoadCustomerBLL2.TabIndex = 3;
            this.btLoadCustomerBLL2.Text = "LoadCustomerBLL2";
            this.btLoadCustomerBLL2.UseVisualStyleBackColor = true;
            this.btLoadCustomerBLL2.Click += new System.EventHandler(this.btLoadCustomerBLL2_Click);
            // 
            // btLoadCustomerBLLGen
            // 
            this.btLoadCustomerBLLGen.Location = new System.Drawing.Point(500, 149);
            this.btLoadCustomerBLLGen.Name = "btLoadCustomerBLLGen";
            this.btLoadCustomerBLLGen.Size = new System.Drawing.Size(142, 23);
            this.btLoadCustomerBLLGen.TabIndex = 4;
            this.btLoadCustomerBLLGen.Text = "LoadCustomerBLLGen";
            this.btLoadCustomerBLLGen.UseVisualStyleBackColor = true;
            this.btLoadCustomerBLLGen.Click += new System.EventHandler(this.button1_Click);
            // 
            // LoadCustomerBLL2Gen
            // 
            this.LoadCustomerBLL2Gen.Location = new System.Drawing.Point(500, 184);
            this.LoadCustomerBLL2Gen.Name = "LoadCustomerBLL2Gen";
            this.LoadCustomerBLL2Gen.Size = new System.Drawing.Size(142, 23);
            this.LoadCustomerBLL2Gen.TabIndex = 5;
            this.LoadCustomerBLL2Gen.Text = "btLoadCustomerBLL2Gen";
            this.LoadCustomerBLL2Gen.UseVisualStyleBackColor = true;
            this.LoadCustomerBLL2Gen.Click += new System.EventHandler(this.btLoadCustomerBLL2Gen_Click);
            // 
            // btAddClasses
            // 
            this.btAddClasses.Location = new System.Drawing.Point(500, 226);
            this.btAddClasses.Name = "btAddClasses";
            this.btAddClasses.Size = new System.Drawing.Size(142, 23);
            this.btAddClasses.TabIndex = 6;
            this.btAddClasses.Text = "AddClasses";
            this.btAddClasses.UseVisualStyleBackColor = true;
            this.btAddClasses.Click += new System.EventHandler(this.btAddClasses_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 397);
            this.Controls.Add(this.btAddClasses);
            this.Controls.Add(this.LoadCustomerBLL2Gen);
            this.Controls.Add(this.btLoadCustomerBLLGen);
            this.Controls.Add(this.btLoadCustomerBLL2);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.btLoadCustomerBLL);
            this.Controls.Add(this.btLoadAssembly);
            this.Name = "Form1";
            this.Text = "For test Task7";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btLoadAssembly;
        private System.Windows.Forms.Button btLoadCustomerBLL;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.Button btLoadCustomerBLL2;
        private System.Windows.Forms.Button btLoadCustomerBLLGen;
        private System.Windows.Forms.Button LoadCustomerBLL2Gen;
        private System.Windows.Forms.Button btAddClasses;
    }
}

