﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7_Test
{
    public interface IFilter<T>
    {
        bool IsValid(T inputType);
    }
}
