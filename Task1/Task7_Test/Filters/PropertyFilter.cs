﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
namespace Task7_Test
{
    public class PropertyFilter:IFilter<Type>
    {
        public bool IsValid(Type inputType)
        {
            return inputType.IsClass && inputType.GetProperties().Any(b => b.GetCustomAttribute<ImportAttribute>() != null);
        }
    }
}
