﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
namespace Task7_Test
{
    public class ClassFilter:IFilter<Type>
    {
        public bool IsValid(Type inputType)
        {
            return  inputType.IsClass && (inputType.GetCustomAttribute<ExportAttribute>(true) != null || inputType.GetCustomAttribute<ImportConstructorAttribute>(true) != null);
        }

    }
}
