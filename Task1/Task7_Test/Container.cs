﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
namespace Task7_Test
{
    public class Container
    {
        
        public Container(IList<IFilter<Type>> listFilters)
        {
            _listFilters = listFilters;
        }
        private IList<IFilter<Type>> _listFilters;
        private Dictionary<Type,Type> _listTypes=new Dictionary<Type, Type>();
        public void AddAssembly(Assembly assembly)
        {
            
            var types = assembly.GetTypes().Where(t => Filter(t));
            foreach(var type in types)
            {
                if(!_listTypes.ContainsKey(type))
                {
                    
                    _listTypes.Add(type, (type.IsClass&&type.GetCustomAttribute<ExportAttribute>() != null )? 
                        type.GetCustomAttribute<ExportAttribute>().Contract : null);
                }
            }
            
        }
        protected bool Filter(Type t)
        {
            foreach (var itm in _listFilters)
            {
                if (itm.IsValid(t))
                {
                    return true;
                }
            }
            return false;
        }
        public void AddType(Type type)
        {
            if (!_listTypes.ContainsKey(type))
            {
                _listTypes.Add(type,
                   (type.IsClass && type.GetCustomAttribute<ExportAttribute>() != null) ?
                        type.GetCustomAttribute<ExportAttribute>().Contract : null);
            }
        }

        public void AddType(Type type, Type baseType)
        {
            if (!_listTypes.ContainsKey(type))
            {
                _listTypes.Add(type, baseType);
            }
        }

        public object CreateInstance(Type type)
        {
            
            if(type.IsClass)
            {
               
                if (type.GetCustomAttribute<ImportConstructorAttribute>() != null)
                {
                    var constructors = type.GetConstructors();
                    if(constructors.Length==1)
                    {
                        var ctor = constructors[0];
                        return ctor.Invoke(ctor.GetParameters().
                            Select(p=> GetParamsValue(p)).ToArray());

                    }
                    else
                    {
                        return null;
                    }
                }
                else if (type.GetProperties().Any(b => b.GetCustomAttribute<ImportAttribute>() != null)
                    || type.GetFields().Any(b => b.GetCustomAttribute<ImportAttribute>() != null))
                {

                    var result = Activator.CreateInstance(type);
                   foreach( var p in type.GetProperties().Where(b => b.GetCustomAttribute<ImportAttribute>() != null))
                    {
                        p.SetValue(result, GetParamsValue(p.PropertyType, a => a.GetCustomAttribute<ExportAttribute>() != null));
                    }
                    foreach (var p in type.GetFields().Where(b => b.GetCustomAttribute<ImportAttribute>() != null))
                    {
                        p.SetValue(result, GetParamsValue(p.FieldType, a => a.GetCustomAttribute<ExportAttribute>() != null));
                    }
                    return result;
                }
            }
            return null;
        }

        public T CreateInstance<T>()where T:class
        {
            return CreateInstance(typeof(T)) as T;
        }
        private Type GetRegisteredType(Type t,Func<Type,bool> filter)
        {
            foreach (var kvp in _listTypes)
            {
                if((kvp.Value==t||kvp.Key==t))
                {
                    if (filter(kvp.Key))
                    {
                        return kvp.Key;
                    }
                }
            }
            return null;
        }
        private object GetParamsValue(ParameterInfo p)
        {
            var result=GetParamsValue(p.ParameterType, a => a.GetCustomAttribute<ExportAttribute>() != null);
            if(result==null)
            {
                
                result = p.HasDefaultValue ? p.DefaultValue : null;
            }
            return result;
        }
        private object GetParamsValue(Type param, Func<Type, bool> filter)
        {         
            Type paramsValueType= GetRegisteredType(param, filter);
            if (paramsValueType != null)
            {
                    return Activator.CreateInstance(paramsValueType);
            }
            return null;
        }
        

    }
}
