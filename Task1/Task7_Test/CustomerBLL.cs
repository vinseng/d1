﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task7_Test
{
    [ImportConstructor]
    public class CustomerBLL
    {
        public CustomerBLL(ICustomerDAL dal, Logger logger)
        {
            this.logger = logger;
            this.CustomerDAL = dal;
        }

        
        public ICustomerDAL CustomerDAL { get; set; }
       
        public Logger logger { get; set; }
    }
}
